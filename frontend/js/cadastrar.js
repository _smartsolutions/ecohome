window.Cadastrar = {

    obrigatorio: function() {
        $("#cadastrar").find("input").focusout(function(){
            var conteudo = $(this).val();

            if(conteudo == ""){
                $(this).prop("placeholder", "obrigatório");
                $(this).addClass("is-invalid");
            }else{
                $(this).removeClass("is-invalid");
            }
        })
    },

    cadastroIrregular: function() {
        return $("#cadastrar").find(".is-invalid").length != 0;
    },

    senhaIrregular: function() {
        let senhaEqual = $("#senha").val() == $("#senhaConfirmar").val();
        if(!senhaEqual){
            $("#senha").addClass("is-invalid");
            $("#senhaConfirmar").addClass("is-invalid");
        }
    },

    buscarInputVazio: function() {
        var lista = $("#cadastrar").find("input");
        for (var i = 0; i < lista.length; i++) {
            var id = lista[i].id;

            if($("#"+ id).val() == ""){
                $("#"+ id).prop("placeholder", "obrigatório");
                $("#"+ id).addClass("is-invalid");
            }
        }
    },

    eventos: function() {
        $("#btnSalvar").click(function(e) {
            Cadastrar.buscarInputVazio();
            Cadastrar.senhaIrregular();

            if(Cadastrar.cadastroIrregular()){
                $("#modal-campos-obrigatorios").modal("show");
                return;
            }else{
                Cadastrar.usuario();
            }
        });
    },

    usuario: function() {
        var nome = $("#nome").val();
        var sobrenome = $("#sobrenome").val();
        var email = $("#email").val();
        var senha = $("#senha").val();
        RestUsuario.create(nome, sobrenome, email, senha, function(err, u) {
            if (err) {
                window.open("cadastrar.html", "_self");
                return;
            }

            SessionStorage.setUser(u._id);
            SessionStorage.setEmail(u.email);

            window.open("index.html", "_self");
        })
    },

    usuarioSessao: function() {
        if(SessionStorage.getUser()){
            window.open("index.html", "_self");
        }
    },

    init: function() {
        this.usuarioSessao();
        this.eventos();
        this.obrigatorio();
    }
}