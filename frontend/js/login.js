window.Login = {

    bloquear: function() {
        $("#inputEmail").prop('readonly', true);
        $("#inputEmail").prop('disabled', true);
    },

    carregarDados: function() {
        $("#inputEmail").val(SessionStorage.getEmail());
    },

    obrigatorio: function() {
        $("#cadastrar").find("input").each(function(){
            var conteudo = $(this).val();

            if(conteudo == ""){
                $(this).prop("placeholder", "obrigatório");
                $(this).addClass("is-invalid");
            }else{
                $(this).removeClass("is-invalid");
            }
        })
    },

    logar: function() {
        var email = $("#inputEmail").val();
        var senha = $("#inputPassword").val();

        if(!email || !senha){
            Login.obrigatorio();
            return;
        }

        if(SessionStorage.getEmail() && SessionStorage.getUser()){
            RestUsuario.read(SessionStorage.getUser(), function(err, u) {
                if (err) {
                    if(err == 404){
                        alert('Usuario não encontrado');
                    }else{
                        alert('Erro');
                    }
                }

                if(senha == u.senha){
                    window.open("dashboard.html", "_self");
                }else{
                    alert('Senha incorreta');
                }
            })
        }else{
            RestUsuario.find('email='+ email, function(err, u) {
                if (err) {
                    if(err == 404){
                        alert('Usuario não encontrado');
                    }else{
                        alert('Erro');
                    }
                    return;
                }

                if(senha == u.senha){
                    SessionStorage.setUser(u._id);
                    SessionStorage.setEmail(u.email);
                    window.open("dashboard.html", "_self");
                }else{
                    alert('Senha incorreta');
                }
            })
        }
    },

    init: function() {
        if(SessionStorage.getEmail() && SessionStorage.getUser()){
            this.bloquear();
            this.carregarDados();
        }
    }
}