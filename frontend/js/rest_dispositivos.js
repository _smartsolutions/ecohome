window.RestDispositivos = {

    create: function(id, ip, nome, descricao, ativo, consumo, callback) {
        jQuery.ajax({
            url: '/app/v1/dispositivos',
            type: "POST",
            data: JSON.stringify({ "usuario": id, "ip": ip, "nome": nome, "descricao": descricao, "ativo": ativo, "consumo": consumo }),
            contentType:"application/json; charset=utf-8",
            success: function (response) {
                callback(null, response);
            },
            error: function (xhr, status, error) {
                callback(xhr.status, null)
            }
        })
    },

    update: function(id, obj, callback) {
        jQuery.ajax({
            url: '/app/v1/dispositivos/' + id,
            type: "PUT",
            data: JSON.stringify(obj),
            contentType:"application/json; charset=utf-8",
            success: function (response) {
                callback(null, response);
            },
            error: function (xhr, status, error) {
                callback(xhr.status, null)
            }
        })
    },

    read: function(id, callback) {
        jQuery.ajax({
            url: '/app/v1/dispositivos/' + id,
            type: "GET",
            success: function (response) {
                callback(null, response);
            },
            error: function (xhr, status, error) {
                callback(xhr.status, null)
            }
        })
    },

    remove: function(id, callback) {
        jQuery.ajax({
            url: '/app/v1/dispositivos/' + id,
            type: "DELETE",
            success: function (response) {
                callback(null, response);
            },
            error: function (xhr, status, error) {
                callback(xhr.status, null)
            }
        })
    },

    find: function(query, callback) {
        jQuery.ajax({
            url: '/app/v1/dispositivos/buscar?'+ query,
            type: "GET",
            success: function (response) {
                callback(null, response);
            },
            error: function (xhr, status, error) {
                callback(xhr.status, null)
            }
        })
    },

    list: function(id, callback) {
        jQuery.ajax({
            url: '/app/v1/dispositivos/listar',
            type: "GET",
            success: function (response) {
                callback(null, response);
            },
            error: function (xhr, status, error) {
                callback(xhr.status, null)
            }
        })
    },

}