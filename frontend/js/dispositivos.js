window.Dispositivos = {

    carregar: function (dados) {
        $("#container-peso").html('<table id="dispositivos" class="display"></table>');

        $('#dispositivos').DataTable({
            responsive: true,
            searching: false,
            paging: false,
            info: false,
            language: {
                url: "js/plugins/datatables-ptbr.json"
            },
            data: dados,
            columns: [
                { title: "ID", responsivePriority: 1 },
                { title: "Usuario", responsivePriority: 1 },
                { title: "IP", responsivePriority: 1 },
                { title: "Nome", responsivePriority: 1 },
                { title: "Descricao", responsivePriority: 1 },
                { title: "Ativo", responsivePriority: 1 },
                { title: "Consumo", responsivePriority: 1 }
            ]
        });
    },

    criarEditar: function() {
        // var peso = $("#input_peso").val();
        // var data = $("#input_data").val();

        // if(peso == ""){
        //     $("#input_peso").addClass("is-invalid");
        // }else{
        //     $("#input_peso").removeClass("is-invalid");
        // }

        // if(data == ""){
        //     $("#input_data").addClass("is-invalid");
        // }else{
        //     $("#input_data").removeClass("is-invalid");
        // }

        // if(peso != "" && data != ""){
        //     this.lista.push([data, peso]);
        //     this.carregar(this.lista);
        //     $("#modal-dados-salvos").modal("show");
        //     $("#input_peso").val("");
        //     $("#input_data").val("");
        // }else{
        //     $("#modal-campos-obrigatorios").modal("show");
        // }
    },

    criar: function() {
        var id = SessionStorage.getUser();
        var ip = $("#input_ip").val();
        var nome = $("#input_nome").val();
        var descricao = $("#input_descricao").val();
        var ativo = eval($("#select_ativo").val());
        var consumo = eval($("#select_consumo").val());

        RestDispositivos.create(id, ip, nome, descricao, ativo, consumo, function(err, res) {
            if (err) {
                if(err == 404){
                    alert('Dispositivo não encontrado');
                }else{
                    alert('Erro');
                }
            }

            alert('Dispositivo criado');
            setTimeout(function(){
                location.reload();
            }, 1000);
        })
    },

    editar: function(id) {
        var obj = {
            ip: $("#input_ip").val(),
            nome: $("#input_nome").val(),
            descricao: $("#input_descricao").val(),
            ativo: eval($("#select_ativo").val()),
            consumo: eval($("#select_consumo").val())
        }

        RestDispositivos.update(id, obj, function(err) {
            if (err) {
                if(err == 404){
                    alert('Dispositivo não encontrado');
                }else{
                    alert('Erro');
                }
            }

            alert('Dispositivo alterado');
            setTimeout(function(){
                location.reload();
            }, 1000);
        })
    },

    eventos: function() {

        $("table#dispositivos").find("tr").on("click", function(e){
            var id = $(this).find("td").eq(0).html();

            RestDispositivos.read(id, function(err, d) {
                if (err) {
                    if(err == 404){
                        alert('Dispositivo não encontrado');
                    }else{
                        alert('Erro');
                    }
                }

                $("#input_id").val(d._id)
                $("#input_id_usuario").val(d.usuario)
                $("#input_ip").val(d.ip)
                $("#input_nome").val(d.nome)
                $("#input_descricao").val(d.descricao)
                $("#select_ativo").val(d.ativo.toString())
                $("#select_consumo").val(d.consumo.toString())

                $("#btn_criarEditar").html("Salvar")
                $("#btn_remover").show();
                $("#btn_remover").attr("href", 'javascript:Dispositivos.remover("'+ id +'")');
                $("#btn_criarEditar").attr("href", 'javascript:Dispositivos.editar("'+ id +'")');

            })

        })

        $("form").find("input").focusout(function(){
            var conteudo = $(this).val();

            if(conteudo == ""){
                $(this).prop("placeholder", "obrigatório");
                $(this).addClass("is-invalid");
            }else{
                $(this).removeClass("is-invalid");
            }
        });

    },

    remover: function(id) {
        RestDispositivos.remove(id, function(err, res) {
            if (err) {
                if(err == 404){
                    alert('Dispositivo não encontrado');
                }else{
                    alert('Erro');
                }
            }

            alert('Dispositivo removido');
            setTimeout(function(){
                location.reload();
            }, 1000);
        })
    },

    restFormat: function(list) {
        var result = [];
        for (var i = 0; i < list.length; i++) {
            var item = list[i];
            result.push([item._id, item.usuario, item.ip, item.nome, item.descricao, item.ativo, item.consumo])
        }
        return result
    },

    rest: function(callback) {
        jQuery.ajax({
            url: "/app/v1/dispositivos/listar",
            success: function (response) {
                callback(Dispositivos.restFormat(response));
            },
            error: function (xhr, status, error) {
                console.log(error);
                callback([])
            }
        })
    },

    init: function() {
        this.rest(function(lista) {
            Dispositivos.carregar(lista);

            setTimeout(function(){
                Dispositivos.eventos();
            }, 500);

        })
    }
}