window.SessionStorage = {

    setEmail: function(email) {
        window.localStorage.setItem('user_email', email);
    },

    getEmail: function() {
        return window.localStorage.getItem('user_email');
    },

    setUser: function(id) {
        window.localStorage.setItem('user_id', id);
    },

    getUser: function() {
        return window.localStorage.getItem('user_id');
    },

    verifyLogged: function() {
        if(!SessionStorage.getUser() && !SessionStorage.getEmail()){
            window.open("index.html", "_self");
        }
    },

    logout: function() {
        window.localStorage.clear();
        window.open("index.html", "_self");
    }
}