window.Usuario = {

    editar: false,

    bloquear: function() {
        $("#usuario").find("input").prop('readonly', true);
        $("#usuario").find("select").prop('disabled', true);
        Usuario.editar = false;
    },

    desbloquear: function() {
        // $("#usuario").find("input").prop('readonly', false);
        // $("#usuario").find("select").prop('disabled', false);

        $("#nome").prop('readonly', false);
        $("#nome").prop('disabled', false);

        $("#sobrenome").prop('readonly', false);
        $("#sobrenome").prop('disabled', false);

        Usuario.editar = true;
    },

    carregarDados: function(u) {
        $("#nome").val(u.nome);
        $("#sobrenome").val(u.sobrenome);
        $("#email").val(u.email);
        $("#senha").val(u.senha);
        $("#senha_confirmar").val(u.senha);
    },

    obrigatorio: function() {
        $("#usuario").find("input").focusout(function(){
            var conteudo = $(this).val();

            if(conteudo == ""){
                $(this).prop("placeholder", "obrigatório");
                $(this).addClass("is-invalid");
            }else{
                $(this).removeClass("is-invalid");
            }
        })
    },

    cadastroIrregular: function() {
        return $("#usuario").find(".is-invalid").length != 0;
    },

    eventos: function() {
        $("#btnEditar").click(function(e) {
            Usuario.desbloquear();
        });

        $("#btnSalvar").click(function(e) {
            if(Usuario.cadastroIrregular()){
                $("#modal-campos-obrigatorios").modal("show");
            }else{
                if(Usuario.editar){
                    Usuario.atualizarCadastro();
                }
            }
        });
    },

    atualizarCadastro: function() {
        var obj = {
            nome: $("#nome").val(),
            sobrenome: $("#sobrenome").val()
        };

        RestUsuario.update(SessionStorage.getUser(), obj, function(err, u) {
            if (err) {
                if(err == 404){
                    alert('Usuario não encontrado');
                }else{
                    alert('Erro');
                }
            }

            $("#modal-dados-salvos").modal("show");
            Usuario.bloquear();
            setTimeout(function(){
                location.reload();
            }, 1000);
        })
    },

    init: function() {
        RestUsuario.read(SessionStorage.getUser(), function(err, u) {
            if (err) {
                if(err == 404){
                    alert('Usuario não encontrado');
                }else{
                    alert('Erro');
                }
            }

            Usuario.eventos();
            Usuario.obrigatorio();
            Usuario.bloquear();
            Usuario.carregarDados(u);
        })
    }
}