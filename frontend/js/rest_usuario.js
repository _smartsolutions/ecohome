window.RestUsuario = {

    create: function(nome, sobrenome, email, senha, callback) {
        jQuery.ajax({
            url: '/app/v1/usuario',
            type: "POST",
            data: JSON.stringify({ "nome": nome, "sobrenome": sobrenome, "email": email, "senha": senha }),
            contentType:"application/json; charset=utf-8",
            success: function (response) {
                callback(null, response);
            },
            error: function (xhr, status, error) {
                callback(xhr.status, null)
            }
        })
    },

    update: function(id, obj, callback) {
        jQuery.ajax({
            url: '/app/v1/usuario/' + id,
            type: "PUT",
            data: JSON.stringify(obj),
            contentType:"application/json; charset=utf-8",
            success: function (response) {
                callback(null, response);
            },
            error: function (xhr, status, error) {
                callback(xhr.status, null)
            }
        })
    },

    read: function(id, callback) {
        jQuery.ajax({
            url: '/app/v1/usuario/' + id,
            type: "GET",
            success: function (response) {
                callback(null, response);
            },
            error: function (xhr, status, error) {
                callback(xhr.status, null)
            }
        })
    },

    find: function(query, callback) {
        jQuery.ajax({
            url: '/app/v1/usuario/buscar?'+ query,
            type: "GET",
            success: function (response) {
                callback(null, response);
            },
            error: function (xhr, status, error) {
                callback(xhr.status, null)
            }
        })
    },

    list: function(id, callback) {
        jQuery.ajax({
            url: '/app/v1/usuario/listar',
            type: "GET",
            success: function (response) {
                callback(null, response);
            },
            error: function (xhr, status, error) {
                callback(xhr.status, null)
            }
        })
    },

}