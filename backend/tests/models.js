const path = require('path');
const wire = require('wire');
const cfg = require('konfig')({ path: 'config' }).config;

const logger = require("winston");
      logger.remove(logger.transports.Console);
      logger.add(logger.transports.Console, cfg.log);

process.on('uncaughtException', function (err) {
    logger.error('Uncaught exception -', err.stack || err)
})

process.on('unhandledRejection', function (err) {
    logger.error('Unhandled Rejection -', err.stack || err);
});

(async () => {
    var dependencies = await wire(require('../services/app/context.js')(cfg), { require: require('../util/require-context.js') })

    let usuario = dependencies.model_usuario;
    let automacao = dependencies.model_automacao;
    let dispositivos = dependencies.model_dispositivos;
    let coleta_consumo = dependencies.model_coleta_consumo;

    /*
        USUARIO
    */
    logger.info('MODEL USUARIO');

    let uc = await usuario.create({ nome: 'blah', sobrenome: 'aaaaa', email: 'blah@gmail.com', senha: 'xxxxx' });
    logger.info( 'CREATE', JSON.stringify(uc) );

    let ur = await usuario.read(uc._id);
    logger.info( 'READ', JSON.stringify(ur) );

    let uu = await usuario.update(uc._id, { nome: 'xxx' });
    logger.info( 'UPDATE', JSON.stringify(uu) );

    let ud = await usuario.delete(uc._id);
    logger.info( 'DELETE', JSON.stringify(ud) );

    //apenas para os testes
    let a_user = await usuario.create({ nome: 'blah', email: 'blah@gmail.com', senha: 'xxxxx' });
    await usuario.delete(a_user._id);

    /*
        AUTOMACAO
    */
    logger.info('MODEL AUTOMACAO');

    let ac = await automacao.create({ usuario: a_user._id, em_casa: true });
    logger.info( 'CREATE', JSON.stringify(ac) );

    let ar = await automacao.read(ac._id);
    logger.info( 'READ', JSON.stringify(ar) );

    let au = await automacao.update(ac._id, { em_casa: false });
    logger.info( 'UPDATE', JSON.stringify(au) );

    let ad = await automacao.delete(ac._id);
    logger.info( 'DELETE', JSON.stringify(ad) );

    /*
        DISPOSITIVOS
    */
    logger.info('MODEL DISPOSITIVOS');

    let dc = await dispositivos.create({ usuario: a_user._id, ip: '127.0.0.1', nome: 'tv', descricao: 'quarto', ativo: true, consumo: false });
    logger.info( 'CREATE', JSON.stringify(dc) );

    let dr = await dispositivos.read(dc._id);
    logger.info( 'READ', JSON.stringify(dr) );

    let du = await dispositivos.update(dc._id, { descricao: 'sala' });
    logger.info( 'UPDATE', JSON.stringify(du) );

    let dd = await dispositivos.delete(dc._id);
    logger.info( 'DELETE', JSON.stringify(dd) );

    /*
        COLETA CONSUMO
    */
    logger.info('MODEL COLETA CONSUMO');

    let cc_dispositivo = await dispositivos.create({ usuario: a_user._id, ip: '127.0.0.1', nome: 'tv', descricao: 'quarto', ativo: true, consumo: false });
    await dispositivos.delete(cc_dispositivo._id);

    let ccc = await coleta_consumo.create({ dispositivo: cc_dispositivo._id, power: 0, factor: 0, voltage: 0, current: 0 });
    logger.info( 'CREATE', JSON.stringify(ccc) );

    let ccr = await coleta_consumo.read(ccc._id);
    logger.info( 'READ', JSON.stringify(ccr) );

    let ccu = await coleta_consumo.update(ccc._id, { power: 100 });
    logger.info( 'UPDATE', JSON.stringify(ccu) );

    let ccd = await coleta_consumo.delete(ccc._id);
    logger.info( 'DELETE', JSON.stringify(ccd) );

    logger.info('Done');
    process.exit(0);
})();