var logger = require('winston');

class GenericErrorHandler {
    static create(...args) {
        return new GenericErrorHandler(...args);
    }

    constructor() {}

    getRouter() {
        logger.debug('Generic error handler');
		return function (err, req, res, next) {
	      	logger.error('Error processing request', req.originalUrl, err.stack || err);
	    	res.status(500).end();
	   	}
    }

}

module.exports = GenericErrorHandler.create;