const path = require('path');
const morgan = require('morgan');
const logger = require('winston');
const express = require('express');
const bodyParser = require('body-parser');

class HttpServerBuilder {
    static create(...args) {
        return new HttpServerBuilder(...args);
    }

    constructor(options, modules) {
        this._options = options;
        this._server = null;
        this._createApp();
        this._installModules(modules);
    }

    _createApp() {
        var app = express();

        app.use(bodyParser.json());
        app.use(express.static('public'));
        app.use(morgan(':remote-addr - :remote-user [:date[iso]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" - :response-time ms'))

        this._app = app;
    }

    getServer() {
        return this._app;
    }

    _installModules(modules) {
        for (var i = 0; i < modules.length; i++) {
            this._app.use(modules[i].getRouter());
        }
    }

    start() {
        logger.info(`Starting ${this._options.name} server on port`, this._options.port);
        this._server = this._app.listen(this._options.port);
    }

    stop() {
        logger.info(`Closing ${this._options.name} server`);
        this._server.close()
    }

}

module.exports = HttpServerBuilder.create;