const mongoose = require('mongoose');
const logger = require('winston');

class MongoClient {

    static create(...args) {
        return new MongoClient(...args);
    }

    constructor(url) {
        return this.client(url);
    }

    async client(url) {
        try{
            let mongo = await mongoose.connect(url, {useNewUrlParser:true});

            mongoose.connection.db.on('error', console.error.bind(logger.error, 'mongoose error:'));

            mongoose.connection.db.once('open', () => {
                logger.info(`Mongodb url=${url} connected`);
            });
            
            return mongo;
        } catch(err) {
            logger.error(`Mongodb url=${url} connection error`);
            throw err;
        }
    }
}

module.exports = MongoClient.create;