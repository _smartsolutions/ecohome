#!/bin/sh

. /lib/lsb/init-functions

SERVICE_NAME="app"
PATH_PID="/var/run/app.pid"
PATH_APP="/opt/app"
PATH_SCRIPT="/opt/app/services/app.js"
PATH_NODE="/opt/node/bin/node"
PATH_LOGS="/var/logs/app.log"


_start() {
  start-stop-daemon --start --quiet --background --no-close \
      --pidfile ${PATH_PID} --make-pidfile \
      --chuid "root" --chdir ${PATH_APP} \
      --exec ${PATH_NODE} -- ${PATH_SCRIPT} >> ${PATH_LOGS} 2>&1
}

_stop() {
  start-stop-daemon --stop --quiet --pidfile ${PATH_PID} --user "root" --signal "TERM"
}

_status() {
  status_of_proc -p ${PATH_PID} ${PATH_NODE} ${SERVICE_NAME}
}

_reload() {
  start-stop-daemon --stop --quiet --pidfile ${PATH_PID} --user "root" --signal "HUP"
}


set -e

start() {
  if _start
  then
    rc=0
    sleep 1
    if ! kill -0 "$(cat ${PATH_PID})" >/dev/null 2>&1; then
        log_failure_msg ${SERVICE_NAME}" failed to start"
        rc=1
    fi
  else
    rc=1
  fi
  if [ "$rc" -eq 0 ]; then
    log_end_msg 0
  else
    log_end_msg 1
    rm -f ${PATH_PID}
  fi
}

export PATH="${PATH:+$PATH:}/usr/sbin:/sbin"

case "$1" in
  start)
    log_daemon_msg "Starting "${SERVICE_NAME}
    if [ -s ${PATH_PID} ] && kill -0 "$(cat ${PATH_PID})" >/dev/null 2>&1; then
      log_progress_msg "apparently already running"
      log_end_msg 0
      exit 0
    fi
    start
  ;;

  stop)
    log_daemon_msg "Stopping "${SERVICE_NAME}
    _stop
    log_end_msg "$?"
    rm -f ${PATH_PID}
  ;;

  reload|force-reload)
    log_daemon_msg "Reloading "${SERVICE_NAME}
    _reload
    log_end_msg "$?"
  ;;

  restart)
    set +e
    log_daemon_msg "Restarting "${SERVICE_NAME}
    if [ -s ${PATH_PID} ] && kill -0 "$(cat ${PATH_PID})" >/dev/null 2>&1; then
      _stop || true
      sleep 1
    else
      log_warning_msg ${SERVICE_NAME}" not running, attempting to start."
      rm -f ${PATH_PID}
    fi
    start
  ;;

  status)
    set +e
    _status
    exit $?
  ;;

  *)
    echo "Usage: /etc/init.d/${SERVICE_NAME} {start|stop|reload|force-reload|restart|status}"
    exit 1
esac

exit 0