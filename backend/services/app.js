const path = require('path');
const wire = require('wire');
const config = require('konfig')({ path: 'config' }).config;

const logger = require("winston");
      logger.remove(logger.transports.Console);
      logger.add(logger.transports.Console, config.log);

process.on('uncaughtException', function (err) {
    logger.error('Uncaught exception -', err.stack || err)
})

process.on('unhandledRejection', function (err) {
    logger.error('Unhandled Rejection -', err.stack || err);
});

wire(require('./app/context.js')(config), { require: require('../util/require-context.js') })
    .then(ctx => {
        logger.debug("Component wiring done");
        ctx.api.start()
    })
    .catch(err => {
        logger.error('Component wiring failed', err.stack || err);
    })