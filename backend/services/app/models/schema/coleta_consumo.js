const mongoose = require('mongoose');

module.exports = function () {
    return {
        dispositivo: {
            type: mongoose.ObjectId,
            required: true
        },
        data: {
            type: Date,
            default: Date.now
        },
        power: {
            type: Number
        },
        factor: {
            type: Number
        },
        voltage: {
            type: Number
        },
        current: {
            type: Number
        }
    }
}