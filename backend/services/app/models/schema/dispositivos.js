const mongoose = require('mongoose');

module.exports = function () {
    return {
        usuario: {
            type: mongoose.ObjectId,
            required: true
        },
        ip: {
            type: String,
            required: true
        },
        nome: {
            type: String,
            required: true
        },
        descricao: {
            type: String,
            required: true
        },
        ativo: {
            type: Boolean
        },
        consumo: {
            type: Boolean
        }
    }
}