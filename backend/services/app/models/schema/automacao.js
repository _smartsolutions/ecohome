const mongoose = require('mongoose');

module.exports = function () {
    return {
        usuario: {
            type: mongoose.ObjectId,
            required: true
        },
        em_casa: {
            type: Boolean,
            required: true
        }
    }
}