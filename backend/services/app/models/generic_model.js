const logger = require("winston");
const mongoose = require('mongoose');
      mongoose.pluralize(null);

class Model {

    /**
     * Método static retorna uma nova instância da classe
     * @argument Array
     */
    static create(...args) {
        return new Model(...args);
    }

    /**
     * Construtor da classe Model
     * @db Object - connection pool do mongodb
     * @collection String - nome da collection
     * @schema Object - Schema no formato do mongoose - https://mongoosejs.com/docs/api.html#Schema
     */
    constructor(db, collection, schema) {
        this._db = db;
        this._schema = new mongoose.Schema(schema, { versionKey: false });

        return this._client(collection, this._schema);
    }

    /**
     * Método private para conectar a collection com o schema
     * @collection String - nome da collection
     * @schema Object - Schema no formato do mongoose - https://mongoosejs.com/docs/api.html#Schema
     */
    async _client(collection, schema) {
        try{
            logger.debug(`Model ${collection} connected`);
            this.client = await this._db.model(collection, schema);
            return this;
        }catch(err){
            logger.error(`Model ${collection} error`);
            throw err;
        }
    }

    /**
     * Cria um novo objeto no mongodb
     * @obj Object - Objeto json com os dados para salvar
     */
    async create(obj) {
        return await this.client(obj).save();
    }

    /**
     * Busca um objeto no mongodb com id como argumento para busca
     * @id String - id do objeto a ser buscado
     */
    async read(id) {
        return await this.client.findOne({ _id: id });
    }

    /**
     * Busca e altera um objeto no mongodb
     * @id String - id do objeto a ser buscado
     * @obj Object - Objeto json com os dados para modificar
     */
    async update(id, obj) {
        return await this.client.updateOne({ _id: id }, { $set: obj }, { runValidators: true });
    }

    /**
     * Deleta um objeto baseado no id
     * @id String - id do objeto
     */
    async delete(id) {
        return await this.client.deleteOne({ _id: id });
    }

    /**
     * Busca 1 resultado com argumento não obrigatório
     * @query Object - Objeto json no formato de query mongodb
     */
    async find(query = {}) {
        return await this.client.findOne(query);
    }

    /**
     * Busca 1 ou mais resultado com argumento não obrigatório
     * @query Object - Objeto json no formato de query mongodb
     */
    async all(query = {}){
        return await this.client.find(query);
    }
}

module.exports = Model.create;