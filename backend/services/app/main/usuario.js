const path = require('path');
const moment = require('moment');
const logger = require('winston');
const _ = require('lodash');

class Usuario {
    static create(...args) {
        return new Usuario(...args);
    }

    constructor(model) {
        // bind dos metodos basicos
        this.create = model.create.bind(model);
        this.read = model.read.bind(model);
        this.update = model.update.bind(model);
        this.delete = model.delete.bind(model);
        this.find = model.find.bind(model);
        this.all = model.all.bind(model);

        logger.debug('Controller handler Usuario')
    }

}

module.exports = Usuario.create;