const logger = require('winston');
const express = require('express');

class Routes {
    static create(...args) {
        return new Routes(...args);
    }

    constructor(controllers) {
        this._router = this._createRouter(controllers);
    }

    _createRouter(controllers) {
        logger.debug('Delivering generic routes');
        var router = express.Router();

        router.get('/app/v1/healthcheck', this.healthcheck);

        // rotas genericas do modelo
        for (let item of controllers) {
            logger.debug(`POST /app/v1/${item.label}`);
            router.post(`/app/v1/${item.label}`, this.promisify(item.model.create.bind(item.model)));

            logger.debug(`GET /app/v1/${item.label}/listar`);
            router.get(`/app/v1/${item.label}/listar`, this.promisify(item.model.list.bind(item.model)));

            logger.debug(`GET /app/v1/${item.label}/buscar`);
            router.get(`/app/v1/${item.label}/buscar`, this.promisify(item.model.find.bind(item.model)));

            logger.debug(`GET /app/v1/${item.label}/:id`);
            router.get(`/app/v1/${item.label}/:id`, this.promisify(item.model.read.bind(item.model)));

            logger.debug(`PUT /app/v1/${item.label}/:id`);
            router.put(`/app/v1/${item.label}/:id`, this.promisify(item.model.update.bind(item.model)));

            logger.debug(`DELETE /app/v1/${item.label}/:id`);
            router.delete(`/app/v1/${item.label}/:id`, this.promisify(item.model.delete.bind(item.model)));
        }

        return router;
    }

    promisify(fn) {
        return function (req, res, next) {
            fn(req, res, next).catch(next)
        }
    }

    healthcheck(req, res) {
        res.status(200).end()
    }

    getRouter() {
        return this._router;
    }
}

module.exports = Routes.create;