class Controller {

    /**
     * Método static retorna uma nova instância da classe
     * @argument Array
     */
    static create(...args) {
        return new Controller(...args);
    }

    /**
     * Construtor da classe Controller
     * @model Object - recebe uma instância de um modelo no formato generic_model.js
     */
    constructor(model) {
        this.model = model;
    }


    /**
     * Usa como dependências o método de mesmo nome do modelo recebido como argumento
     * @req Object - https://expressjs.com/en/4x/api.html#req
     * @res Object - https://expressjs.com/en/4x/api.html#res
     */
    create(req, res) {
        return this.model.create(req.body).then((item) => {
            if (item) {
                res.status(200).json(item);
            } else {
                res.status(404).end()
            }
        });
    }

    /**
     * Usa como dependências o método de mesmo nome do modelo recebido como argumento
     * @req Object - https://expressjs.com/en/4x/api.html#req
     * @res Object - https://expressjs.com/en/4x/api.html#res
     */
    read(req, res) {
        return this.model.read(req.params.id).then((item) => {
            if (item) {
                res.status(200).json(item);
            } else {
                res.status(404).end()
            }
        });
    }

    /**
     * Usa como dependências o método de mesmo nome do modelo recebido como argumento
     * @req Object - https://expressjs.com/en/4x/api.html#req
     * @res Object - https://expressjs.com/en/4x/api.html#res
     */
    update(req, res) {
        return this.model.update(req.params.id, req.body).then((item) => {
            if (item) {
                res.status(200).json(item);
            } else {
                res.status(404).end()
            }
        });
    }

    /**
     * Usa como dependências o método de mesmo nome do modelo recebido como argumento
     * @req Object - https://expressjs.com/en/4x/api.html#req
     * @res Object - https://expressjs.com/en/4x/api.html#res
     */
    delete(req, res) {
        return this.model.delete(req.params.id).then((item) => {
            if (item) {
                res.status(200).json(item);
            } else {
                res.status(404).end()
            }
        });
    }

    /**
     * Usa como dependências o método de mesmo nome do modelo recebido como argumento
     * @req Object - https://expressjs.com/en/4x/api.html#req
     * @res Object - https://expressjs.com/en/4x/api.html#res
     */
    list(req, res) {
        return this.model.all(req.query).then((item) => {
            if (item) {
                res.status(200).json(item);
            } else {
                res.status(404).end()
            }
        });
    }

    /**
     * Usa como dependências o método de mesmo nome do modelo recebido como argumento
     * @req Object - https://expressjs.com/en/4x/api.html#req
     * @res Object - https://expressjs.com/en/4x/api.html#res
     */
    find(req, res) {
        return this.model.find(req.query).then((item) => {
            if (item) {
                res.status(200).json(item);
            } else {
                res.status(404).end()
            }
        });
    }

}

module.exports = Controller.create;