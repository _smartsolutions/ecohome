const path = require('path');

/*
    Responsável por instanciar as classes do sistema
    @module Object - arquivo ou módulo a ser carregado
    @args Array - Argumentos do construtor da classe
*/
module.exports = function (cfg) { return {

    db: {
        create: {
            module: '../util/mongo-client',
            args: [
                cfg.mongo.url,
            ]
        }
    },

    /*
        USUARIO
    */
    schema_usuario: {
        create: {
            module: '../services/app/models/schema/usuario',
            args: []
        }
    },

    model_usuario: {
        create: {
            module: '../services/app/models/generic_model',
            args: [
                { $ref: 'db' },
                'usuario',
                { $ref: 'schema_usuario' }
            ]
        }
    },

    main_usuario: {
        create: {
            module: '../services/app/main/usuario',
            args: [
                { $ref: 'model_usuario' }
            ]
        }
    },

    controller_usuario: {
        create: {
            module: '../services/app/api/generic_controller',
            args: [ 
                { $ref: 'main_usuario' }
            ]
        }
    },

    /*
        AUTOMACAO
    */
    schema_automacao: {
        create: {
            module: '../services/app/models/schema/automacao',
            args: []
        }
    },

    model_automacao: {
        create: {
            module: '../services/app/models/generic_model',
            args: [
                { $ref: 'db' },
                'automacao',
                { $ref: 'schema_automacao' }
            ]
        }
    },

    main_automacao: {
        create: {
            module: '../services/app/main/automacao',
            args: [
                { $ref: 'model_automacao' }
            ]
        }
    },

    controller_automacao: {
        create: {
            module: '../services/app/api/generic_controller',
            args: [ 
                { $ref: 'main_automacao' }
            ]
        }
    },

    /*
        DISPOSITIVOS
    */
    schema_dispositivos: {
        create: {
            module: '../services/app/models/schema/dispositivos',
            args: []
        }
    },

    model_dispositivos: {
        create: {
            module: '../services/app/models/generic_model',
            args: [
                { $ref: 'db' },
                'dispositivo',
                { $ref: 'schema_dispositivos' }
            ]
        }
    },

    main_dispositivos: {
        create: {
            module: '../services/app/main/dispositivos',
            args: [
                { $ref: 'model_dispositivos' }
            ]
        }
    },

    controller_dispositivos: {
        create: {
            module: '../services/app/api/generic_controller',
            args: [ 
                { $ref: 'main_dispositivos' }
            ]
        }
    },

    /*
        COLETA CONSUMO
    */
    schema_coleta_consumo: {
        create: {
            module: '../services/app/models/schema/coleta_consumo',
            args: []
        }
    },

    model_coleta_consumo: {
        create: {
            module: '../services/app/models/generic_model',
            args: [
                { $ref: 'db' },
                'coleta_consumo',
                { $ref: 'schema_coleta_consumo' }
            ]
        }
    },

    main_coleta_consumo: {
        create: {
            module: '../services/app/main/coleta_consumo',
            args: [
                { $ref: 'model_coleta_consumo' }
            ]
        }
    },

    controller_coleta_consumo: {
        create: {
            module: '../services/app/api/generic_controller',
            args: [ 
                { $ref: 'main_coleta_consumo' }
            ]
        }
    },

    /*
        API
    */
    routes: {
        create: {
            module: '../services/app/api/routes',
            args: [ 
                [
                    { label: 'usuario', model: { $ref: 'controller_usuario' } },
                    { label: 'automacao', model: { $ref: 'controller_automacao' } },
                    { label: 'dispositivos', model: { $ref: 'controller_dispositivos' } },
                    { label: 'coleta-consumo', model: { $ref: 'controller_coleta_consumo' } }
                ]
            ]
        }
    },

    genericErrorHandler: {
        create: {
            module: '../util/generic-error-handler'
        }
    },

    api: {
        create: {
            module: '../util/http-server-builder',
            args: [ 
                cfg.services.api,
                [
                    { $ref: 'routes' },
                    { $ref: 'genericErrorHandler' }
                ]
            ]
        }
    }

} };