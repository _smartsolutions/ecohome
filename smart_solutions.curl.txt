/app/v1/usuario:

Create:
curl -X POST \
  http://34.201.45.32:3005/app/v1/usuario \
  -H 'Content-Type: application/json' \
  -d '{
    "nome": "lorem",
    "sobrenome": "ipsum",
    "email": "lorem@ipsum.com",
    "senha": "123"
}'

Read:
curl -X GET http://34.201.45.32:3005/app/v1/usuario/5cb343b4e2f36b0bbc821fec

Update:
curl -X PUT \
  http://34.201.45.32:3005/app/v1/usuario/5ce2f8baa33d0b0d20efc198 \
  -H 'Content-Type: application/json' \
  -d '{
    "nome": "lorem",
    "sobrenome": "ipsum",
    "email": "lorem@ipsum.com",
    "senha": "123456"
}'

Delete:
curl -X DELETE http://34.201.45.32:3005/app/v1/usuario/5ce2fb87a33d0b0d20efc19a

List:
curl -X GET http://34.201.45.32:3005/app/v1/usuario/listar

Find:
curl -X GET 'http://34.201.45.32:3005/app/v1/usuario/buscar?nome=Felipe'


/app/v1/automacao:

Create:
curl -X POST \
  http://34.201.45.32:3005/app/v1/automacao \
  -H 'Content-Type: application/json' \
  -d '{
    "usuario": "5cb343b4e2f36b0bbc821fec",
    "em_casa": true
}'

Read:
curl -X GET http://34.201.45.32:3005/app/v1/automacao/5ce2fd69a33d0b0d20efc19b

Update:
curl -X PUT \
  http://34.201.45.32:3005/app/v1/automacao/5ce2fd69a33d0b0d20efc19b \
  -H 'Content-Type: application/json' \
  -d '{
    "usuario": "5cb343b4e2f36b0bbc821fec",
    "em_casa": false
}'

Delete:
curl -X DELETE http://34.201.45.32:3005/app/v1/automacao/5ce2fdf8a33d0b0d20efc19d

List:
curl -X GET http://34.201.45.32:3005/app/v1/automacao/listar

Find:
curl -X GET 'http://34.201.45.32:3005/app/v1/automacao/buscar?usuario=5cb343b4e2f36b0bbc821fec'

/app/v1/dispositivos:

Create:
curl -X POST \
  http://34.201.45.32:3005/app/v1/dispositivos \
  -H 'Content-Type: application/json' \
  -d '{
    "usuario": "5cb343b4e2f36b0bbc821fec",
    "ip": "127.0.0.1",
    "nome": "tv",
    "descricao": "quarto",
    "ativo": true,
    "consumo": false
}'

Read:
curl -X GET http://34.201.45.32:3005/app/v1/dispositivos/5ce2ff64a33d0b0d20efc19e

Update:
curl -X PUT \
  http://34.201.45.32:3005/app/v1/dispositivos/5ce2ff64a33d0b0d20efc19e \
  -H 'Content-Type: application/json' \
  -d '{
    "_id": "5ce2ff64a33d0b0d20efc19e",
    "usuario": "5cb343b4e2f36b0bbc821fec",
    "ip": "127.0.0.1",
    "nome": "tv",
    "descricao": "quarto",
    "ativo": true,
    "consumo": false
}'

Delete:
curl -X DELETE http://34.201.45.32:3005/app/v1/dispositivos/5ce2ffe5a33d0b0d20efc1a0

List:
curl -X GET http://34.201.45.32:3005/app/v1/dispositivos/listar

Find:
curl -X GET 'http://34.201.45.32:3005/app/v1/dispositivos/buscar?usuario=5cb343b4e2f36b0bbc821fec'

/app/v1/coleta-consumo:

Create:
curl -X POST \
  http://34.201.45.32:3005/app/v1/coleta-consumo \
  -H 'Content-Type: application/json' \
  -d '{
    "dispositivo": "5cb35ab6a33d0b0d20efc193",
    "power": 0,
    "factor": 0,
    "voltage": 0,
    "current": 0
}'

Read:
curl -X GET http://34.201.45.32:3005/app/v1/coleta-consumo/5ce3010ba33d0b0d20efc1a1

Update:
curl -X PUT \
  http://34.201.45.32:3005/app/v1/coleta-consumo/5ce3010ba33d0b0d20efc1a1 \
  -H 'Content-Type: application/json' \
  -d '{
    "dispositivo": "5cb35ab6a33d0b0d20efc193",
    "power": 1,
    "factor": 1,
    "voltage": 1,
    "current": 1
}'

Delete:
curl -X DELETE http://34.201.45.32:3005/app/v1/coleta-consumo/5ce30152a33d0b0d20efc1a3

List:
curl -X GET http://34.201.45.32:3005/app/v1/coleta-consumo/listar

Find:
curl -X GET 'http://34.201.45.32:3005/app/v1/coleta-consumo/buscar?dispositivo=5cb35ab6a33d0b0d20efc193'